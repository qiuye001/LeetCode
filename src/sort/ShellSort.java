package sort;

/**
 * ϣ������
 * 
 * @author XIAO
 *
 */

public class ShellSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] arr = { 10, 1, 5, 2, 7, 12, 9, 8, 4, 16, 6 };
		int len = arr.length / 2;
		while (len > 0) {
			sort(arr, len);
			len = len / 2;
		}

		for (int m = 0; m < arr.length; m++) {
			System.out.print(" " + arr[m]);
		}
	}

	public static void sort(int[] a, int len) {
		int i = 0;
		while (i + len <= a.length - 1) {
			if (a[i] > a[i + len]) {
				int temp = a[i];
				a[i] = a[i + len];
				a[i + len] = temp;
			}
			i++;
		}

	}
}
