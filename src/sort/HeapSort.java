package sort;

/**
 * 堆排序
 * 
 * @author XIAO
 *
 */
public class HeapSort {

	private static int arr[] = { 1, 4, 7, 23, 14, 27, 2, 8, 88 };

	public static void main(String[] args) {
		for (int i = 0; i < arr.length; i++) {
			MinHeapFixUp(arr, i);
		}

		for (int i = 0; i < arr.length; i++) {
			MinHeapFixDown(arr, i);
		}

		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}

	/**
	 * 建立 小顶堆
	 * 
	 * @param a
	 * @param i
	 */
	public static void MinHeapFixUp(int[] a, int i) {
		// parent
		int p = (i - 1) / 2;
		// the value want to insert
		int temp = a[i];
		while (p >= 0 && a[p] > temp) {
			a[i] = a[p];
			a[p] = temp;
			i = p;
			p = (i - 1) / 2;
		}
	}

	/**
	 * 把最小元素交换到末尾，重新调整顶堆
	 * 
	 * @param a
	 * @param i
	 */
	public static void MinHeapFixDown(int[] a, int i) {
		// put the min to last
		int last = a.length - i - 1;
		int temp = a[last];
		a[last] = a[0];
		a[0] = temp;

		// rebuild the heap
		int k = 0;
		while (k < last) {
			int p = k;
			int min = a[k];
			if (2 * k + 1 < last && a[2 * k + 1] < min) {
				min = a[2 * k + 1];
				p = 2 * k + 1;
			}

			if (2 * k + 2 < last && a[2 * k + 2] < min) {
				min = a[2 * k + 2];
				p = 2 * k + 2;
			}
			// 父节点与子节点交换
			if (p > k && p < last) {
				temp = a[p];
				a[p] = a[k];
				a[k] = temp;
				k = p;
			} else {
				break;
			}

		}

	}
}
