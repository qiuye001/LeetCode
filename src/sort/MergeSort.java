package sort;

/**
 * 归并排序
 * 
 * @author XIAO
 *
 */
public class MergeSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] arr = { 10, 1, 5, 2, 7, 12, 9, 8, 4, 16, 6 };
		int[] b = new int[arr.length];
		sort(arr, 0, arr.length - 1, b);
		for (int m = 0; m < arr.length; m++) {
			System.out.print(" " + arr[m]);
		}

	}

	/**
	 * 合并两段已排序部分
	 * 
	 * @param a
	 * @param l
	 *            开始位置
	 * @param mid
	 *            中间位置
	 * @param r
	 *            结束位置
	 * @param b
	 *            用于辅助，临时存储排序的内容
	 */
	public static void merge(int a[], int l, int mid, int r, int[] b) {
		int i = l;
		int j = mid + 1;
		int n = 0;
		// 合并
		while (i <= mid && j <= r) {
			if (a[i] <= a[j]) {
				b[n++] = a[i++];
			} else {
				b[n++] = a[j++];
			}
		}

		while (i <= mid) {
			b[n++] = a[i++];
		}
		while (j <= r) {
			b[n++] = a[j++];
		}

		for (int m = 0; m < n; m++) {
			a[l + m] = b[m];
		}
	}

	/**
	 * 递归调用-归并排序
	 * 
	 * @param a
	 * @param l
	 * @param r
	 * @param b
	 */
	public static void sort(int[] a, int l, int r, int[] b) {

		if (l < r) {
			int mid = (l + r) / 2;

			// 排序左边部分
			sort(a, l, mid, b);
			// 排序右边部分
			sort(a, mid + 1, r, b);

			// 两部分合并
			merge(a, l, mid, r, b);
		}

	}

}
