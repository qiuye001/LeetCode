package sort;

/**
 * 快速排序
 * 
 * @author XIAO
 *
 */
public class QuickSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] arr = { 1, 6, 2, 5, 7, 3, 3 };
		;

		int i = 0;
		int j = arr.length - 1;
		sort(arr, i, j);
		for (int k = 0; k < arr.length; k++) {
			System.out.print(arr[k] + " ");
		}

	}

	/**
	 * 递归调用，一轮排序，确定一个位置
	 * 
	 * @param arr
	 * @param l
	 *            开始位置
	 * @param r
	 *            结束位置
	 * @return
	 */
	public static void sort(int[] arr, int l, int r) {
		int i = l;
		int j = r;
		if (i >= j)
			return;
		int x = arr[l];

		while (i < j) {
			while (i < j && x < arr[j]) {
				j--;
			}
			if (i < j) {

				arr[i] = arr[j];
				arr[j] = x;
				i++;
			}

			while (i < j && x > arr[i]) {
				i++;
			}
			if (i < j) {
				arr[j] = arr[i];
				arr[i] = x;
				j--;
			}

		}
		// i左边的排序
		sort(arr, l, i - 1);
		
		// i右边的排序
		sort(arr, i + 1, r);
	}

}
