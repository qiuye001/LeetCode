package sort;

/**
 * 简单插入排序
 * 
 * @author XIAO
 *
 */
public class StraightInsertionSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = { 10, 1, 5, 2, 7, 12, 9, 8, 4, 10, 6 };
		for (int i = 0; i < arr.length; i++) {
			int j = i - 1;

			// temp 为待排序数字
			int temp = arr[i];
			// k为 temp所在位置
			int k = i;

			// 从后向前查找待插入位置。将前面比temp大的数字与其对换位置
			while (j >= 0 && temp < arr[j]) {
				arr[k--] = arr[j--];
				arr[k] = temp;
			}

		}

		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);
		}
	}

}
