package sort;

/**
 * 冒泡排序
 * 
 * @author XIAO
 *
 */
public class BubbleSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] arr = { 10, 9, 8, 7, 6, 5, 11 };
		// 用于标记是否发生了交换
		boolean flag = false;
		for (int i = 0; i < arr.length; i++) {
			flag = false;
			for (int j = 0; j < arr.length - i - 1; j++) {
				// 与相邻位置比较，确定是否发生交换行为
				if (arr[j] > arr[j + 1]) {
					int temp = arr[j + 1];
					arr[j + 1] = arr[j];
					arr[j] = temp;
					flag = true;
				}
			}
			// 如果一轮比较结束，没有发生交换位置的行为，说明已经排好序了
			if (!flag) {
				break;
			}
		}
		for (int m = 0; m < arr.length; m++) {
			System.out.print(" " + arr[m]);
		}

	}

}
