package sort;

import java.util.ArrayList;

/**
 * 基数排序
 * 
 * @author XIAO
 *
 */
public class RadixSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] arr = { 10, 1, 5, 2, 7, 12, 9, 8, 4, 10, 6 };
		int m = 1;
		while (sort(arr, m) == false) {
			m = m * 10;
		}

		for (int k = 0; k < arr.length; k++) {
			System.out.print(" " + arr[k]);
		}
	}

	@SuppressWarnings("unchecked")
	public static boolean sort(int[] a, int m) {
		// 标记是否已经排到最高位
		boolean flag = false;
		// 建立十个桶
		ArrayList<Integer>[] t = new ArrayList[10];
		for (int i = 0; i < 10; i++) {
			t[i] = new ArrayList<Integer>();
		}
		// 对每个位置上的数字取余，放入对应的桶中
		for (int i = 0; i < a.length; i++) {
			int k = (a[i] / m) % (10);
			t[k].add(a[i]);
		}

		// 将桶中按一位排好的数字，依次取出，放入数组，等待下一次排序
		int i = 0;
		for (ArrayList<Integer> l : t) {
			for (Integer k : l) {
				a[i++] = k;
			}
		}

		if (t[0].size() == a.length) {
			flag = true;
		}

		return flag;
	}
}
