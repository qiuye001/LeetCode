/**
 * Sort a linked list in O(n log n) time using constant space complexity.
 * 对链表使用归并排序。
 * 
 * @author XIAO
 *
 */

/**
 * Definition for singly-linked list.
 */
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
		next = null;
	}
}

public class Sort_List {
	/**
	 * 对一个单链表进行归并排序
	 * 
	 * @param head
	 * @return
	 */
	public ListNode sortList(ListNode head) {
		// 如果是空值，或者只有一个元素，直接返回
		if (null == head || null == head.next) {
			return head;
		}
		// fast节点一次走两步，slow节点一次走一步，确定中心节点位置
		ListNode fast = head;
		ListNode slow = head;
		while (null != fast && null != fast.next && null != fast.next.next) {
			fast = fast.next.next;
			slow = slow.next;
		}
		// 将链表分成两段，分别标记开头节点（lHead，rHead）和结束位置节点（lEnd，rEnd）
		ListNode lHead = null;
		ListNode rHead = null;
		ListNode lEnd = null;
		ListNode rEnd = null;

		// 确定分成两段链表后起始位置
		if (null == fast.next) {
			lHead = head;
			lEnd = slow;
			rHead = slow.next;
			rEnd = fast;
		} else if (null == fast.next.next) {
			lHead = head;
			lEnd = slow;
			rHead = slow.next;
			rEnd = fast.next;
		}

		// 将链表分成两段后，尾部节点的后一个节点置null
		lEnd.next = null;
		rEnd.next = null;
		// 两段链表分别排序
		lHead = sortList(lHead);
		rHead = sortList(rHead);

		// 合并两个链表
		head = mergeList(lHead, rHead);
		return head;
	}

	/**
	 * 两个有序的链表合并
	 * 
	 * @param lHead
	 * @param rHead
	 * @return
	 */
	private ListNode mergeList(ListNode lHead, ListNode rHead) {
		// 前节点
		ListNode pre = null;
		// 头结点
		ListNode mergeHead = null;

		while (lHead != null && rHead != null) {

			if (lHead.val <= rHead.val) {
				// 如果前节点(或者头结点)还未初始化位置，则进行初始化赋值
				if (null == pre) {
					mergeHead = lHead;
					pre = lHead;
					lHead = lHead.next;
				} else {
					pre.next = lHead;
					lHead = lHead.next;
					pre = pre.next;
				}

			} else {
				if (null == pre) {
					mergeHead = rHead;
					pre = rHead;
					rHead = rHead.next;
				} else {
					pre.next = rHead;
					rHead = rHead.next;
					pre = pre.next;
				}
			}

		}// _end while

		// 如果一链表结束，将另一链表的剩余值加入尾部
		if (null == lHead) {
			pre.next = rHead;
		} else {
			pre.next = lHead;
		}
		return mergeHead;
	}

	public static void main(String[] args) {
		Sort_List s = new Sort_List();
		ListNode head = new ListNode(5);
		ListNode pre = head;
		pre.next = new ListNode(7);
		pre = pre.next;
		pre.next = new ListNode(3);
		pre = pre.next;
		pre.next = new ListNode(8);
		pre = pre.next;
		pre.next = new ListNode(15);
		pre = pre.next;
		pre.next = new ListNode(1);
		pre = pre.next;
		pre.next = new ListNode(13);
		pre = pre.next;
		pre.next = new ListNode(4);
		pre = pre.next;
		pre.next = new ListNode(29);
		pre = pre.next;
		pre.next = null;
		head = s.sortList(head);

		while (head != null) {
			System.out.print(" " + head.val);
			head = head.next;
		}
	}

}
