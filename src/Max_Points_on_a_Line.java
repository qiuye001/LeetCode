import java.util.HashMap;
import java.util.Map;

/**
 * Max Points on a Line(最多共线点的数目) 
 * Given n points on a 2D plane, find the maximum number of points that lie on the same straight line
 * 
 * @author XIAO
 *
 */

/**
 * Definition for a point.
 */
class Point {
	int x;
	int y;

	Point() {
		x = 0;
		y = 0;
	}

	Point(int a, int b) {
		x = a;
		y = b;
	}
}

public class Max_Points_on_a_Line {
	public int maxPoints(Point[] points) {
		// 如果点的数目小于3个，直接返回点的个数
		if (points.length < 3) {
			return points.length;
		}
		// 最大共线点数
		int result = 0;

		Map<Double, Integer> slopeMap = new HashMap<Double, Integer>();
		for (int i = 0; i < points.length; i++) {
			slopeMap.clear();
			// 与i重合的点个数
			int samePomint = 0;
			// 与i共线的最大点个数
			int pointMax = 1;

			// 从 i点以后的点开始计算，之前的已经算过了
			for (int j = i + 1; j < points.length; j++) {
				// 斜率 null 为 Y轴斜率
				Double slope = null;

				// 计算斜率
				if (points[i].x == points[j].x) {
					// 与i点共点
					if (points[i].y == points[j].y) {
						samePomint++;
						continue;
					}
				} else {
					// 斜率
					slope = (double) (points[i].y - points[j].y)
							/ (double) (points[i].x - points[j].x);
				}

				// double 中 -0.0 和0.0 的归化
				if (null != slope && -0.0 == slope) {
					slope = 0.0;
				}
				// 更新相同斜率下的共线点数
				int count = 0;
				if (slopeMap.get(slope) != null) {
					slopeMap.put(slope, slopeMap.get(slope) + 1);
					count = slopeMap.get(slope);
				} else {
					slopeMap.put(slope, 2);
					count = 2;
				}

				// 更新i点共线的最大共线点数
				if (count > pointMax) {
					pointMax = count;
				}
			}
			// 更新全局共线最大点数，记得加上相同的点
			result = result > (pointMax + samePomint) ? result : pointMax
					+ samePomint;

		}

		return result;
	}

	public static void main(String[] args) {
		Max_Points_on_a_Line m = new Max_Points_on_a_Line();
		Point[] points = new Point[3];
		points[0] = new Point(0, 0);
		points[1] = new Point(1, 1);
		points[2] = new Point(1, -1);

		int s = m.maxPoints(points);
		System.out.println(s);

	}

}
