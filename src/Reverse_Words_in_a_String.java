/**
 * Reverse Words in a String 
 * Given an input string, reverse the string word by word.
 * 
 * For example, 
 * Given s = "the sky is blue", 
 * return "blue is sky the".
 * 
 * @author XIAO
 *
 */
public class Reverse_Words_in_a_String {
	
	public String reverseWords(String s) {
		if (null == s || 0 == s.length()) {
			return s;
		}
		String[] words = s.trim().split("\\s+");
		StringBuilder sb = new StringBuilder();
		for (int i = words.length - 1; i >= 0; i--) {
			sb.append(words[i]);
			if (i != 0) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		Reverse_Words_in_a_String r = new Reverse_Words_in_a_String();
		System.out.println(r.reverseWords("the sky is blue"));
	}
}
