import java.util.Stack;

/**
 * Evaluate the value of an arithmetic expression in Reverse Polish Notation.
 * 
 * Valid operators are +, -, *, /. Each operand may be an integer or another
 * expression.
 * 
 * Some examples: 
 * ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9 
 * ["4","13","5", "/", "+"] -> (4 + (13 / 5)) -> 6
 * ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]->(10*(6/((9+3)*(-11)))+17+5)->22
 * 
 * @author XIAO
 *
 */
public class Evaluate_Reverse_Polish_Notation {
	public int evalRPN(String[] tokens) {
		Stack<Integer> stack = new Stack<Integer>();
		int i = 0;
		while (i < tokens.length) {
		
			switch (tokens[i]) {
			case "+":
				stack.push(stack.pop() + stack.pop());
				break;
			case "-":
				Integer subtractor = stack.pop();
				stack.push(stack.pop() - subtractor);
				break;
			case "*":
				stack.push(stack.pop() * stack.pop());
				break;
			case "/":
				Integer divisor = stack.pop();
				stack.push(stack.pop() / divisor);
				break;
			
			default:
				//如果不是上述的符号，则直接把数字压入栈中
				stack.push(Integer.valueOf(tokens[i]));
				break;
			}
			i++;
		}

		return stack.pop();
	}

	public static void main(String[] args) {
		Evaluate_Reverse_Polish_Notation e = new Evaluate_Reverse_Polish_Notation();
		String[] tokens = {"10","6","9","3","+","-11","*","/","*","17","+","5","+"};
		System.out.println(e.evalRPN(tokens));
	}
}
