/**
 * Insertion Sort List. 
 * Sort a linked list using insertion sort.
 * 
 * @author XIAO
 *
 */

public class Insertion_Sort_List {
	/**
	 * Definition for singly-linked list.
	 */

	public class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
			next = null;
		}
	}

	/**
	 * 链表的插入排序
	 * 
	 * @param head
	 * @return
	 */
	public ListNode insertionSortList(ListNode head) {
		// 如果链表为空，或者只有一个元素，直接返回
		if (null == head || null == head.next) {
			return head;
		}
		ListNode p = head;
		ListNode q = head.next;
		// 将链表分成两段
		head.next = null;
		// 从另一链表中分离出第一个节点，加入head链表中
		while (null != q) {
			p = head;
			// 寻找q插入的位置
			// q插入头结点位置
			if (q.val < head.val) {
				head = q;
				q = q.next;
				head.next = p;
			} else {
				// 非头结点位置
				ListNode pre = p;
				p = p.next;
				// 确定插入位置
				while (null != p && p.val <= q.val) {
					pre = p;
					p = p.next;
				}

				// 在末尾插入
				if (null == p) {
					pre.next = q;
					q = q.next;
					pre.next.next = null;
				} else {
					// 在中间插入
					pre.next = q;
					q = q.next;
					pre.next.next = p;
				}
			}
		}
		return head;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Insertion_Sort_List sl = new Insertion_Sort_List();
		ListNode head = sl.new ListNode(8);
		ListNode p = head;
		ListNode q = sl.new ListNode(3);
		p.next = q;
		p = p.next;
		q = sl.new ListNode(2);
		p.next = q;
		p = p.next;
		q = sl.new ListNode(7);
		p.next = q;
		p = p.next;
		q = sl.new ListNode(5);
		p.next = q;
		p = p.next;
		q = sl.new ListNode(11);
		p.next = q;
		p = p.next;
		q = sl.new ListNode(2);
		p.next = q;
		p = p.next;
		q = sl.new ListNode(6);
		p.next = q;
		p = p.next;

		head = sl.insertionSortList(head);
		while (null != head) {
			System.out.print(" " + head.val);
			head = head.next;
		}

	}

}
