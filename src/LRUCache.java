import java.util.HashMap;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache. It
 * should support the following operations: get and set.
 * 
 * get(key) - Get the value (will always be positive) of the key if the key
 * exists in the cache, otherwise return -1.
 * 
 * set(key, value) - Set or insert the value if the key is not already present.
 * When the cache reached its capacity, it should invalidate the least recently
 * used item before inserting a new item.
 * 
 * 双链表 + hashtable实现原理： 
 * 
 * 将Cache的所有位置都用双连表连接起来，当一个位置被命中之后，就将通过调整链表的指向， 将该位置调整到链表头的位置，新加入的Cache直接加到链表头中。 
 * 这样，在多次进行Cache操作后，最近被命中的，就会被向链表头方向移动， * 而没有命中的，而想链表后面移动，链表尾则表示最近最少使用的Cache。
 * 当需要替换内容时候，链表的最后位置就是最少被命中的位置，我们只需要淘汰链表最后的部分即可。
 * 
 * @author XIAO
 *
 */
public class LRUCache {

	// 设计容量
	private int capacity;
	// 缓存容器
	private HashMap<Integer, Node> nodes;
	// 当前容量
	private int currentSize;
	// 头结点和尾节点
	private Node head;
	private Node last;

	// 初始化cache
	public LRUCache(int capacity) {
		currentSize = 0;
		this.capacity = capacity;
		// 缓存容器初始化
		nodes = new HashMap<Integer, Node>(capacity);
	}

	/**
	 * 获取缓存中的值，并将它放到链表最前面
	 * 
	 * @param key
	 * @return
	 */
	public int get(int key) {
		Node node = nodes.get(key);
		if (null != node) {
			moveToHead(node);
			return node.value;
		} else {
			return -1;
		}
	}

	/**
	 * 将一个节点移动到链表首部
	 * 
	 * @param node
	 */
	private void moveToHead(Node node) {
		if (node == head) {
			return;
		}
		// 把node节点 提取出来
		if (null != node.prev) {
			node.prev.next = node.next;
		}
		if (node.next != null) {
			node.next.prev = node.prev;
		}
		// 检测是否是最后节点位置
		if (last == node) {
			last = node.prev;
		}

		// 插入头结点位置
		if (null != head) {
			node.next = head;
			head.prev = node;
		}
		// 头结点没有前节点
		head = node;
		head.prev = null;
		// 如果是第一次加入缓存，头结点就是尾节点
		if (null == last) {
			last = head;
		}

	}

	/**
	 * 添加一个新的缓存
	 * 
	 * @param key
	 * @param value
	 */
	public void set(int key, int value) {

		Node node = nodes.get(key);
		// 如果 该缓存不存在
		if (null == node) {
			if (currentSize >= capacity) {
				// 移除最近最少使用的缓存
				removeLast();
			} else {
				currentSize++;
			}
			// 新建一个缓存节点
			node = new Node();
		}
		// 将最新使用的节点放到链表头，表示最新使用过的.
		node.key = key;
		node.value = value;
		nodes.put(key, node);
		moveToHead(node);
	}

	/**
	 * 移除链表最后一个节点
	 */
	private void removeLast() {
		nodes.remove(last.key);

		if (null != last) {
			if (null != last.prev) {
				last.prev.next = null;
			} else {
				// 只有一个节点，被移除了
				head = null;
			}
			// last 节点重定位
			last = last.prev;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LRUCache lc = new LRUCache(3);
		lc.set(1, 1);
		lc.set(2, 2);
		lc.set(3, 3);
		lc.set(4, 4);
		lc.set(5, 5);
		lc.set(1, 1);
		lc.set(2, 2);
		lc.set(4, 4);

		System.out.println(lc.get(5));
	}

	// 节点结构示意
	class Node {
		Node prev;
		Node next;
		int value;
		int key;
	}
}
