/**
 * You are given two non-empty linked lists representing two non-negative
 * integers. The digits are stored in reverse order and each of their nodes
 * contain a single digit. Add the two numbers and return it as a linked list.
 * You may assume the two numbers do not contain any leading zero, except the
 * number 0 itself. 
 * Example: 
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4) 
 * Output: 7 -> 0 -> 8 
 * Explanation: 342 + 465 = 807.
 * 
 * @author xiaohe
 *
 */

public class AddTwoNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AddTwoNum atn = new AddTwoNum();
		ListNode l1 = new ListNode(8);
		l1.next = new ListNode(9);
		l1.next.next = new ListNode(9);
		// l1.next.next.next = new ListNode(1);

		ListNode l2 = new ListNode(2);
		 l2.next = new ListNode(9);
		// l2.next.next = new ListNode(4);
		// l2.next.next.next = new ListNode(1);

		l1 = atn.addTwoNumbers(l1, l2);
		while (l1 != null) {
			System.out.print(l1.val);
			l1 = l1.next;
		}
	}

	/**
	 * Definition for singly-linked list. public class ListNode { int val; ListNode
	 * next; ListNode(int x) { val = x; } }
	 */

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		//如果L1，L2有一个是空的，直接返回另一个非空链表节点
		if (l1 == null) {
			return l2;
		} else if (l2 == null) {
			return l1;
		}
		//add为进位值
		int add = 0;
		ListNode head = l1;
		//在L1,L2走到最后一个节点前，两者数值相加，保存到L1节点中，不产生额外新的保存链表。
		while (l1.next != null && l2.next != null) {
			l1.val = l1.val + l2.val + add;
			add = l1.val / 10;
			l1.val = l1.val % 10;
			l1 = l1.next;
			l2 = l2.next;
		}
		//如果L1,L2同时走到了最后一个节点。
		if (l1.next == null && l2.next == null) {
			l1.val = l1.val + l2.val + add;
			add = l1.val / 10;
			l1.val = l1.val % 10;
			if (add > 0) {
				l1.next = new ListNode(add);
			}
		} else {
			//L1和L2中的某个链表走到了最后一个节点，如果是L1走到了末尾，直接接管L2剩下的节点，这样，只剩下L1和进位了。
			if (l1.next == null && l2.next != null) {
				l1.next = l2.next;
			}
			l1.val = l1.val + l2.val + add;
			add = l1.val / 10;
			l1.val = l1.val % 10;
			l1 = l1.next;
			//L1剩下节点的进位处理，直到最后节点
			while (l1.next != null) {
				l1.val = l1.val + add;
				add = l1.val / 10;
				l1.val = l1.val % 10;
				l1 = l1.next;
			}
			//最后一个节点的进位处理。
			if (l1.next == null) {
				l1.val = l1.val + add;
				add = l1.val / 10;
				l1.val = l1.val % 10;
				if (add > 0) {
					l1.next = new ListNode(add);
				}

			}

		}

		return head;
	}

}
