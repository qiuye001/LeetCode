/**
 * Given an array of integers, return indices of the two numbers such that they
 * add up to a specific target. You may assume that each input would have
 * exactly one solution, and you may not use the same element twice.
 * 
 * Given nums = [2, 7, 11, 15], target = 9, Because nums[0] + nums[1] = 2 + 7 =
 * 9, return [0, 1].
 * 
 * @author xiaohe
 * 
 */
public class TwoSum {
	public static void main(String[] args) {
		int[] nums = { 2, 7, 11, 15 };
		int target = 9;
		int[] ret = twoSum(nums, target);
		System.out.print(ret[0] + "  " + ret[1]);
	}

	// ������ֵѰ�ҡ�
	public static int[] twoSum(int[] nums, int target) {
		int[] ret = null;
		for (int i = 0; i < nums.length; i++) {
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[j] == target - nums[i]) {
					ret = new int[] { i, j };
				}
			}
		}
		return ret;
	}
}
